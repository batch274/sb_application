package com.zuitt.wdc044.services;

import com.zuitt.wdc044.models.User;
import com.zuitt.wdc044.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

// "@Service" Annotation is used to indicate that it holds the actual business logic.
@Service
public class UserServiceImpl implements UserService{
    // "@Autowired" is used to access objects and methods of another class.
    @Autowired
    private UserRepository userRepository;


    // Create user
    public void createUser(User user){
        userRepository.save(user);
    }

    //if user exists
    public Optional<User> findByUsername(String username){
        return Optional.ofNullable(userRepository.findByUsername(username));
    }
}
